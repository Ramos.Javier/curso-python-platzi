# Curso-Python-Platzi

Curso basico de python en Platzi.

## Mod 1 - Básicos de programación

* ¿Por qué programar con Python?
* ¿Qué es la programación?
* ¿Python 2 vs Python 3?
* ¿Operadores matemáticos en Python?
* ¿Tipos de datos en Python?
* Declarar variables y expresiones
* Definir funciones con Python
* Funciones con parámetros
* Calcular si un número es primo con Python
* Estructura de condicionales en Python
* Buenas prácticas del lenguaje

## Mod 2 - Uso de strings y ciclos

* Comparación de strings y unicode
* Factorial de un número con recursión
* Manejo de strings en Python
* Separar cadenas de texto en Python
* Ciclos en Python con for
* Ciclos en Python con while
* Calcular si una palabra es palíndromo con Python

## Mod 3 - Estructuras de datos

* Introducción a las listas en Python
* Operaciones con listas en Python
* Juego del ahorcado con Python
* Interfaz del ahorcado
* Lógica del ahorcado
* Qué es una búsqueda binaria
* Implementar busqueda binaria en Python
* Diccionarios en Python
* Encriptar mensajes usando diccionarios
* Tuplas en Python
* Se repite una letra en un string - Programa con tuplas
* Uso de sets en Python
* Dictionary comprehension - list comprehension

## Mod 4 - NO DISPONIBLE

## Mod 5 - Proyecto Web Scraping

* ¿Qué es web scraping?
* Implementando web scraping con Python

## Mod 6 - Proyecto Aplicacion de Terminal

* Interfaz directorio de contactos
* Lógica de directorio de contactos
* Persistencia de datos

## Mod 7 - Proyecto Aplicacion Web

* Configurando el entorno de desarrollo de Google Cloud
* Crear templates con Jinja2
* Manejar rutas y lógica con Flask
* Listar y eliminar contactos

## Mod 8 - Practicas Comunes

* Charla de cierre
