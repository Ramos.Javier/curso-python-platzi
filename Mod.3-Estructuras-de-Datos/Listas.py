
def average_temps(temps):
    sum_of_temps = 0
    for temp in temps:
        sum_of_temps += float(temp)

    return sum_of_temps/len(temps)

if __name__ == '__main__':
    
    temps = [21,24,24,22,20,23,24]
    average = average_temps(temps)
    print( 'La temperatura promedio es {}'.format(average) )


    mi_lista_nombre = list()
    mi_lista_apellido = list()

    mi_lista_nombre.append('Javier')
    mi_lista_nombre.append('Alexander')
    mi_lista_apellido.append('Ramos')
    mi_lista_apellido.append('Aranibar')

    print(mi_lista_nombre + mi_lista_apellido)
    print(mi_lista_nombre[0]*5)
    print(mi_lista_apellido[0])

    mi_lista_apellido.pop()
    print(mi_lista_apellido)

    nombre_apellido = ' '.join(mi_lista_nombre+mi_lista_apellido)
    print(nombre_apellido)
