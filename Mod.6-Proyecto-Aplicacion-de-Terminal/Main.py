import csv
from ContactBook import ContactBook

def run():

    contacts_book = ContactBook()
    
    with open('contacts.csv', 'r') as f:
        reader = csv.reader(f)
        for idx, row in enumerate(reader):
            if idx == 0:
                continue
            contacts_book.add(row[0], row[1], row[2])

    while True:
        command = str(input('''
            ¿ Que desea hacer ?

            [a]ñadir contacto
            [ac]ualizar contacto
            [b]uscar contacto
            [e]liminar contacto
            [l]istar contactos
            [s]alir
        '''))

        if command == 'a':
            name = str(input('Ingrese el nombre del contacto : '))
            phono = str(input('Ingrese el telefono del contacto : '))
            email = str(input('Ingrese el email del contacto : '))
            
            contacts_book.add(name, phono, email)
            
        elif command == 'ac':
            name = str(input('Ingrese el nombre del contacto a actualizar : '))
            
            contacts_book.update(name)
            
        elif command == 'b':
            name = str(input('Ingrese el nombre del contacto a buscar : '))
            
            contacts_book.search(name)
            
        elif command == 'e':
            name = str(input('Ingrese el nombre del contacto a eliminar : '))
            contacts_book.delete(name)
        elif command == 'l':
            contacts_book.show_all()
        elif command == 's':
            break
        else:
            print('')

if __name__ == '__main__':
    print('BIENVENIDO A LA AGENDA')
    run()
