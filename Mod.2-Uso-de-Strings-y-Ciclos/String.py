
string = 'JAVIER'

print(len(string))
print(string.lower())
print(string.upper())
print(string[0])
print(string[1])
print(string[2])
print(string[3])
print(string.find('E'))

cadena = 'platzi'

print(cadena[1:])
print(cadena[1:3])
print(cadena[1:5])
print(cadena[1:6:2])
print(cadena[::-1])
