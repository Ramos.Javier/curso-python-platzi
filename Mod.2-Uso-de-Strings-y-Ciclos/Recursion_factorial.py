
def factorial(numero):
    if numero == 0:
        return 1
    return numero * factorial(numero-1)

def run():

    numero = int(input('Ingresa un numero: '))
    result = factorial(numero)
    print('El factorial del numero {} es {}.'.format(numero, result))

if __name__ == '__main__':
    run()
