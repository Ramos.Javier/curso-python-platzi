
def palindrome(palabra):
    reversed_word = palabra[::-1]
    if reversed_word == palabra:
        return True
    else:
        return False

if __name__ == '__main__':
    palabra = str(input('Ingresa una palabra: '))
    if palindrome(palabra):
        print('Es un palindromo.')
    else:
        print('No es un palindromo.')
