
def run():
    print('CALCULADORA DE DIVISAS')
    print('Convierte pesos mexicanos a pesos colombianos')
    print('')

    ammount = float(input('Ingresa cantidad de pesos mexicanos: '))
    result = foreign_exchange_calculator(ammount)
    print('${} pesos mexicanos son ${} pesos colombianos'.format(ammount, result))

def foreign_exchange_calculator(ammount):
    mex_to_col_rate = 145.97
    return ammount * mex_to_col_rate

if __name__ == '__main__':
    run()
