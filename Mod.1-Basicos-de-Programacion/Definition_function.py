import turtle

## Definir funciones

def main():
    window = turtle.Screen()
    javier = turtle.Turtle()
    make_rectangle(javier)
    
    turtle.mainloop()

def make_rectangle(javier):
    length = int(input('¿Cúal es el tamaño del cuadrado? '))
    for i in range(4):
        make_line_and_turn(javier, length)

def make_line_and_turn(javier, length):
    javier.forward(length)
    javier.left(90)

if __name__ == '__main__':
    main()